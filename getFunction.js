const obj = {
  a: {
    b: {
      c: 'd',
    },
    e: 'f',
  },
};

const get = (obj, path, defaultValue = undefined) => {
  if (path === undefined || path === null) {
    return obj;
  }
  const parts = path.split('.');
  return parts.reduce((object, key) => object?.[key], obj) || defaultValue;
};

console.log(get(obj, 'a.x.e', 'My default value'));
